# Einfache Wärmeleitungsgleichung

## Anforderungen:

* Das ausgewählte Problem mit mindestens zwei parallelen Schnittstellen zu implementieren (Projekt-spezifische Abweichungen sind in Rücksprache mit dem Dozenten möglich)
* Die Implementation fehlerfrei zu gestalten (valgrind und helgrind bzw. address sanitizer und thread-sanitizer)
* Initialisierungen per Kommandozeileninterface (oder Konfigurationsdatei) zuzulassen
* Der berechneten Konfigurationen zu visualisieren
* Die Dauer der Ausführung der Implementationen in Abhängigkeit steigender Problemgröße auszuwerten
* Gute Dokumentation des Quelltextes (doxygen)
* Besonders positiv bewertet wird auch ein benutzerfreundliches Interface mit Eingriffsmöglichkeiten (Parameterveränderungen, Perturbationen, etc.)

## Project

This repository contains two versions of our project. 

The first one was realised with QT. It has a GUI and allows you to define a converging temperature (targetE) at which the algorithm stops calculating.

The second one is a simpler solution which we implemented afterwards to have the basic algorithm in a 3D array without any object structure. We wanted to have all the temperature values stored next to each other in one variable. This allowed us to improve the algorithm and create two parallel approaches which will fasten up the calculation process.
