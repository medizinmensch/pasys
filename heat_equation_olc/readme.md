# Config

## Tools-Setup

In this version we have implemented two parallel approaches: 
* OpenMP 
* Intel Intrinsics (using AVX).

You will need to have OpenMP installed and your CPU needs to support AVX to make use of these optimisations. 

### Windows
We used Visual Studio for execution. You just need to make sure you create a C++ project and copy the ```olcPixelGameEngine.h``` file into yor project. Our VS project file is included in the repo. 
If you need help or want to do a different approach check the [wiki](https://github.com/OneLoneCoder/olcPixelGameEngine/wiki).

To find out about your supported CPU features check [this software](https://www.cpuid.com/softwares/cpu-z.html).

### Ubuntu

```
sysctl -a | grep machdep.cpu.features

sudo apt update
sudo apt install build-essential libglu1-mesa-dev libpng-dev
```

### Arch

```
sysctl -a | grep machdep.cpu.features

sudo pacman -Sy
sudo pacman -S gcc glibc libpng mesa
```

### Mac

You should follow [this setup](https://github.com/MumflrFumperdink/olcPGEMac) to be able to run the PixelGame Engine on Mac in XCode. In the Build Settings you should also tick the option to "Enable additional vector extensions" so it allows AVX Intrinsics.

To have a list of supported CPU features: 
```sysctl -a | grep machdep.cpu.features```


## Compile and Run
To use the Intel Intrinsics you maybe also need to use the argument to compile the program for your specific cpu-type with ```-march=cpu-type```. You can check the available types [here](https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html).

### On Arch (and Ubuntu?) Linux

```
g++ -o he_pge.out main.cpp -lX11 -lGL -lpthread -lpng -lstdc++fs -std=c++17 -fopenmp && ./he_pge.out
```


### Command line arguments

You can start the program without any arguments however you are able to set parameters when executing the application:

In Visual Studio you can set the cmd arguments under Project Properties -> Debugging -> Command Arguments.
```
./he_pge.out <size> <depth> <gui || nogui || benchmark> <intrinsics || nointrinsics> <openmp noopenmp> 
```


## Benchmark

We did the benchmarking on windows using a .bat script (```benchmark.bat```) and the x64 release version of the project. 
With the command line arguments we could easily switch the different parallel optimisations on and off.
