qmake heat_equation.pro CONFIG+=debug CONFIG+=console
mkdir build
make
make clean
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~   Starting program   ~~~~~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

./heat_equation.app/Contents/MacOS/heat_equation 101 0.01 nogui
./heat_equation.app/Contents/MacOS/heat_equation 101 0.001 nogui
