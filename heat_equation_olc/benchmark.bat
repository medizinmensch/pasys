@echo off

SET /A size=402
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark no-intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark no-intrinsics openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark intrinsics openmp


SET /A size=802
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark no-intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark no-intrinsics openmp
.\x64\Release\heat_equation_olc.exe %size% 400 benchmark intrinsics openmp

.\x64\Release\heat_equation_olc.exe %size% 2000 benchmark no-intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 2000 benchmark intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 2000 benchmark no-intrinsics openmp
.\x64\Release\heat_equation_olc.exe %size% 2000 benchmark intrinsics openmp

.\x64\Release\heat_equation_olc.exe %size% 4000 benchmark no-intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 4000 benchmark intrinsics no-openmp
.\x64\Release\heat_equation_olc.exe %size% 4000 benchmark no-intrinsics openmp
.\x64\Release\heat_equation_olc.exe %size% 4000 benchmark intrinsics openmp