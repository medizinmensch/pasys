#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string>
#include <unordered_map>
#include <omp.h>
#include <immintrin.h>

using namespace std;

bool benchmark = false;
bool enableGui = true;
bool intrinsics = false;
bool openMP = false;

float*** tdarray(int depth, int size, int temp, bool gui, bool intrinsics, bool openMP) {

	std::clock_t start;
	start = clock();
	int i, j, k;

	// Allocate 3D Array
	float*** array3D = new float** [depth];
	for (i = 0; i < depth; i++)
	{
		array3D[i] = new float* [size];
		for (j = 0; j < size; j++)
			array3D[i][j] = new float[size];
	}
	double allocateDuration = (clock() - start) / (double)CLOCKS_PER_SEC;

	// Init
	start = clock();
	for (i = 0; i < depth; i++)
	{
		for (j = 0; j < size; j++)
		{
			for (k = 0; k < size; k++)
				array3D[i][j][k] = 0;
		}
	}
	array3D[0][size / 2][size / 2] = temp;
	double initDuration = (clock() - start) / (double)CLOCKS_PER_SEC;

	// Calc
	start = clock();
	float c = 0.2;
	int increment = 1;

	if (intrinsics)
		increment = 8;

	for (i = 1; i < depth; i++)
	{
		#pragma omp parallel for private(k, j) num_threads(4) if (openMP)
		for (j = 1; j < size - 1; j = j + 1)
		{
			if (i == 1 && j == 1 && openMP && omp_get_num_threads() == 1) 
				cout << "Warning: OpenMP inactive" << endl;

			for (k = 1; k < size - 1;)
			{	
				// when pixels in rows are less then 8, will also use "normal" method instead of intrinsics
				if (!intrinsics || size % 8 != 0 && k > size - increment) {
					array3D[i][j][k] = array3D[i - 1][j][k] +
						c * (array3D[i - 1][j][k + 1] + array3D[i - 1][j][k - 1] - 2 * array3D[i - 1][j][k]) +
						c * (array3D[i - 1][j + 1][k] + array3D[i - 1][j - 1][k] - 2 * array3D[i - 1][j][k]);
				}
				else {
					__m256 c = _mm256_set1_ps(0.2);
					__m256 oldCenterValues = _mm256_setr_ps(array3D[i - 1][j][k], array3D[i - 1][j][k + 1], array3D[i - 1][j][k + 2], array3D[i - 1][j][k + 3],
						array3D[i - 1][j][k + 4], array3D[i - 1][j][k + 5], array3D[i - 1][j][k + 6], array3D[i - 1][j][k + 7]);

					__m256 rightValues = _mm256_setr_ps(array3D[i - 1][j][k + 1], array3D[i - 1][j][k + 2], array3D[i - 1][j][k + 3], array3D[i - 1][j][k + 4],
						array3D[i - 1][j][k + 5], array3D[i - 1][j][k + 6], array3D[i - 1][j][k + 7], array3D[i - 1][j][k + 8]);
					__m256 leftValues = _mm256_setr_ps(array3D[i - 1][j][k - 1], array3D[i - 1][j][k], array3D[i - 1][j][k + 1], array3D[i - 1][j][k + 2],
						array3D[i - 1][j][k + 3], array3D[i - 1][j][k + 4], array3D[i - 1][j][k + 5], array3D[i - 1][j][k + 6]);
					__m256 upperValues = _mm256_setr_ps(array3D[i - 1][j + 1][k], array3D[i - 1][j + 1][k + 1], array3D[i - 1][j + 1][k + 2], array3D[i - 1][j + 1][k + 3],
						array3D[i - 1][j + 1][k + 4], array3D[i - 1][j + 1][k + 5], array3D[i - 1][j + 1][k + 6], array3D[i - 1][j + 1][k + 7]);
					__m256 lowerValues = _mm256_setr_ps(array3D[i - 1][j - 1][k], array3D[i - 1][j - 1][k + 1], array3D[i - 1][j - 1][k + 2], array3D[i - 1][j - 1][k + 3],
						array3D[i - 1][j - 1][k + 4], array3D[i - 1][j - 1][k + 5], array3D[i - 1][j - 1][k + 6], array3D[i - 1][j - 1][k + 7]);

					__m256 doubleOldCenterValues = _mm256_add_ps(oldCenterValues, oldCenterValues);
					__m256 horizontalValues = _mm256_add_ps(rightValues, leftValues);
					horizontalValues = _mm256_sub_ps(horizontalValues, doubleOldCenterValues);
					horizontalValues = _mm256_mul_ps(horizontalValues, c);
					__m256 verticalValues = _mm256_add_ps(upperValues, lowerValues);
					verticalValues = _mm256_sub_ps(verticalValues, doubleOldCenterValues);
					verticalValues = _mm256_mul_ps(verticalValues, c);
					__m256 result = _mm256_add_ps(oldCenterValues, horizontalValues);
					result = _mm256_add_ps(result, verticalValues);

					_mm256_store_ps(&array3D[i][j][k], result);
				}

				if (intrinsics && k + increment < size)
					k += increment;
				else
					k += 1;
			}
		}
		array3D[i][size / 2][size / 2] = temp;
	}
	double calcDuration = (clock() - start) / (double)CLOCKS_PER_SEC;

	start = clock();

	// Deallocate 3D array
	if (!gui) {
		for (i = 0; i < depth; i++)
		{
			for (j = 0; j < size; j++)
				delete[] array3D[i][j];
			delete[] array3D[i];
		}
		delete[] array3D;

	}
	double deleteDuration = (clock() - start) / (double)CLOCKS_PER_SEC;

	std::cout << depth << "/" << size << "/" << size << "," << size * size * depth << "," << allocateDuration << "," << initDuration << "," << calcDuration << "," << deleteDuration << "," << intrinsics << "," << openMP << endl;
	return array3D;
}

std::tuple<int, int, int> getColorRelativeToMaxTemp(int currTemp, int maxTemp) {
	if (currTemp == 0) // zero is always black
		return std::make_tuple(0, 0, 0);
	else if (currTemp == maxTemp) // max temp is always white
		return std::make_tuple(255, 255, 255);
	else if (currTemp > (maxTemp * 0.6))
		return std::make_tuple(255, 255, 100);
	else if (currTemp > (maxTemp * 0.4))
		return std::make_tuple(255, 200, 100);
	else if (currTemp > (maxTemp * 0.25))
		return std::make_tuple(255, 64, 0);
	else if (currTemp > (maxTemp * 0.1))
		return std::make_tuple(192, 64, 0);
	else if (currTemp > (maxTemp * 0.05))
		return std::make_tuple(192, 128, 0);
	else if (currTemp > (maxTemp * 0.025))
		return std::make_tuple(128, 128, 0);
	else if (currTemp > (maxTemp * 0.01))
		return std::make_tuple(64, 128, 64);
	else if (currTemp > (maxTemp * 0.005))
		return std::make_tuple(0, 128, 96);
	else if (currTemp > (maxTemp * 0.001))
		return std::make_tuple(0, 64, 96);
	else if (currTemp > (maxTemp * 0.0001))
		return std::make_tuple(0, 64, 128);
	else if (currTemp > (maxTemp * 0.00001))
		return std::make_tuple(0, 32, 96);
	else if (currTemp > (maxTemp * 0.000001))
		return std::make_tuple(0, 32, 64);
	else
		return std::make_tuple(0, 0, 64);
}

class HeatGradient : public olc::PixelGameEngine
{
public:
	float*** arr;
	int size;
	int depth;
	int maxTemp;
	int activeFrame = 0;
	bool intrinsics;
	bool openMP;

	HeatGradient(int sizeXY, int depthZ, int maxTempHot, bool intelIntrinsics, bool openmp)
	{
		sAppName = "Heat equation";
		size = sizeXY;
		depth = depthZ;
		maxTemp = maxTempHot;
		intrinsics = intelIntrinsics;
		openMP = openmp;
	}

	bool OnUserCreate() override
	{
		size = ScreenWidth();
		arr = tdarray(depth, size, maxTemp, true, intrinsics, openMP);
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		if (GetMouseWheel() > 0)
			activeFrame = activeFrame + 1;
		if (GetMouseWheel() < 0) activeFrame--;
		if (activeFrame < 0)
			activeFrame = 0;
		if (activeFrame >= depth)
			activeFrame = depth - 1;

		for (int x = 0; x < size; x++)
			for (int y = 0; y < size; y++) {
				std::tuple<int, int, int> rgbColors = getColorRelativeToMaxTemp(arr[activeFrame][y][x], maxTemp);
				Draw(x, y, olc::Pixel(get<0>(rgbColors), get<1>(rgbColors), get<2>(rgbColors)));
			}

		return true;
	}
};

int main(int argc, char* argv[]) {

	// parse command line arguments and use default values if none are given

	int size = 100;
	char* p;
	int depth = 100;
	int maxTemp = 1000;

	if (argc >= 3) {
		size = strtol(argv[1], &p, 10);
		depth = atof(argv[2]);
	}
	if (argc >= 4 && strcmp(argv[3], "nogui") == 0)
		enableGui = false;
	else if (argc >= 4 && strcmp(argv[3], "benchmark") == 0) {
		benchmark = true;
		enableGui = false;
	}
	if (argc >= 5 && strcmp(argv[4], "intrinsics") == 0)
		intrinsics = true;
	if (argc >= 6 && strcmp(argv[5], "openmp") == 0)
		openMP = true;

	cout << "Settings: " << "GUI: " << enableGui << ", Intrinsics: " << intrinsics << ", openMP: "<< openMP << ", Benchmark: " << benchmark <<  endl;
	cout << "z/y/x,Squares,Allocate,Init,Calc,Delete,intrinsics,openmp" << endl;

	// decide between gui or nogui mode and calculate tdarray
	if (enableGui) {
		HeatGradient heatGradient(size, depth, maxTemp, intrinsics, openMP);
		if (heatGradient.Construct(size, size, 2, 2))
			heatGradient.Start();
	}
	else if (benchmark) {
		for (int y = 0; y < 4; y++) {
			tdarray(depth, size, maxTemp, enableGui, intrinsics, openMP);
		}
	}
	else {
		tdarray(depth, size, maxTemp, enableGui, intrinsics, openMP);
	}
	return 0;
}
