echo "Building..."
g++ -o heat_equation_olc_linux_executable main.cpp -lX11 -lGL -lpthread -lpng -lstdc++fs -std=c++17 -fopenmp -march=knl 
echo "Build done. Executeable is 'heat_equation_olc_linux_executable'"

echo "Executing with size 100/100/100 without intrinsicts or openmp"
./heat_equation_olc_linux_executable 100 100 nogui no-intrinsics no-openmp
echo "done"
