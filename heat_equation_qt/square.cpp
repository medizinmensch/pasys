#include "square.h"
#include "QDebug"

Square::Square() {
}

Square::Square(double temperature, bool holdStatic) {
  this->temperature = temperature;
  this->holdStatic = holdStatic;
  this->e = 100000;
}

QDebug operator<<(QDebug debug, const Square &sq) {
  QDebugStateSaver saver(debug);
  debug.nospace() << "t:" << sq.temperature << (sq.holdStatic ? "t" : "f")
				  << sq.e;
  return debug;
}
