#include "window.h"
#include "renderarea.h"

#include <QtWidgets>

const int allowedDecimalsForE = 3;

Window::Window(int size, double e, int maxTemp, bool guiMode, bool openmp) {
  renderArea = new RenderArea(size, e, maxTemp, openmp);
  renderArea->targetE = e;

  // Setup for GUI elements if 'guiMode' is enabled
  if (guiMode) {

      frameSlider = new QSlider(Qt::Horizontal);
      frameSlider->setTickPosition(QSlider::TicksBelow);
      frameSlider->setTickInterval(5);
      frameSlider->setDisabled(true);

      frameSpinBox = new QSpinBox();
      frameSpinBox->setDisabled(true);
      frameLabel = new QLabel(tr("&Frame:"));
      frameLabel->setStyleSheet("font-weight: bold");
      frameLabel->setBuddy(frameSlider);

      currentMaxE = new QLabel("0");
      currentMaxELabel = new QLabel("Current Max e:");
      currentMaxELabel->setStyleSheet("font-weight: bold");
      currentMaxELabel->setBuddy(currentMaxE);

      // connectors/slots
      connect(frameSlider, &QSlider::valueChanged, renderArea,
              &RenderArea::sliderChanged);

      connect(frameSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this,
              &Window::frameSpinBoxChanged);

      connect(frameSlider, QOverload<int>::of(&QSlider::valueChanged), this,
              &Window::frameSliderChanged);

      // Layout
      QGridLayout *mainLayout = new QGridLayout;

      mainLayout->addWidget(renderArea, 0, 0, 20, 20);

      // bottom row:
      mainLayout->addWidget(frameLabel, 21, 0, 1, 1);
      mainLayout->addWidget(frameSlider, 21, 1, 1, 14);
      mainLayout->addWidget(frameSpinBox, 21, 15, 1, 1);

      mainLayout->addWidget(currentMaxELabel, 21, 17, 1, 2, Qt::AlignRight);
      mainLayout->addWidget(currentMaxE, 21, 19, 1, 1);

      setLayout(mainLayout);
      setWindowTitle(tr("Heat Equation calculation"));
  }

  // Start process of calculation for temperature frames until convergence for given e is reached
  this->calculate(guiMode);
}

void Window::frameSpinBoxChanged(int value) {
  renderArea->frameSpinBoxChanged(value);
  frameSlider->setValue(value);
}

void Window::frameSliderChanged(int value) {
  double e = renderArea->frameSpinBoxChanged(value);
  currentMaxE->setText(QString::number(e, 'f', allowedDecimalsForE));
  frameSpinBox->setValue(value);
}

void Window::sizeChanged(int value) {
  qDebug() << "size changed: " << value;
  renderArea->boundarySize = value;
  // delete renderArea;
}

void Window::calculate(bool guiMode) {
  QElapsedTimer timer;
  timer.start();
  renderArea->calculate();
  double elapsed = timer.elapsed();

  // one line console log for benchmarking
  qDebug() << "Dimensions:" << renderArea->boundarySize << "- targeted E:" << renderArea->targetE << "- calculated Frames:" << renderArea->availableFrames << "- elapsed Time (in ms):" << elapsed;

  // show popup with information about calculation process and set values accordingly in GUI mode
  if (guiMode) {
      frameSpinBox->setMaximum(renderArea->availableFrames);
      frameSlider->setMaximum(renderArea->availableFrames);
      frameSpinBox->setDisabled(false);
      frameSlider->setDisabled(false);

      QString info = "Calculated '";
      info.append(QString::number(renderArea->availableFrames));
      info.append("' frames for a '");
      info.append(QString::number(renderArea->boundarySize));
      info.append("x");
      info.append(QString::number(renderArea->boundarySize));
      info.append("' area. \nGiven 'e' for conversion: ");
      info.append(QString::number(renderArea->targetE));
      info.append("\nProcess took ");
      if (elapsed < 1000) {
        info.append(QString::number(elapsed));
        info.append(" milliseconds.");
      } else {
        info.append(QString::number(elapsed / 1000));
        info.append(" seconds.");
      }

      QMessageBox::information(this, tr("Calculation finished"),
                               tr(info.toLocal8Bit().data()));
  }

}
