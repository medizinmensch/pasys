qmake heat_equation_qt.pro CONFIG+=debug CONFIG+=console
make
make clean

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~   Starting program   ~~~~~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

./heat_equation_qt 101 0.01
# ./heat_equation_qt 101 0.01 nogui openmp
