#include <iostream>
#include <stdlib.h>
#include "window.h"

#include <QApplication>

using namespace std;

int main(int argc, char *argv[]) {
  Q_INIT_RESOURCE(heat_equation);

   bool guiMode = true;
   bool openmp = false;

   int dimensions;
   double targetE;
   int maxTemp = 200;
   char *p;

   // parse command line arguments and use default values if none are given
   if (argc < 3) {
     dimensions = 51;
     targetE = 0.01;
   }
   else {
     dimensions = strtol(argv[1], &p, 10);
     targetE = atof(argv[2]);
   }
   if (argc >= 4 && strcmp(argv[3], "nogui") == 0)
     guiMode = false;

   if (argc >= 5 && strcmp(argv[4], "openmp") == 0)
     openmp = true;

  QApplication app(argc, argv);
  Window window(dimensions, targetE, maxTemp, guiMode, openmp);

  // render window and show if guiMode is set
  if (guiMode) {
      QDesktopWidget dw;
      window.show();
      return app.exec();
  }
  else {
      //qDebug() << "No gui will be shown. Execution is just used for benchmarking.";
      return 1;
  }


}
