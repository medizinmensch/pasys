# Config

## Tools-Setup

We used Qt Creator for developing this project.
Download from [here](https://www.qt.io/download-qt-installer).

We also used OpenMP and tried to improve the calculation speed however for this version it is not necessary as this version focuses on usability.

## Compile and Run

You can either build in the IDE itself or use the command line. For CMD we also provided shell scripts (```buildAndRunProgram<OS>.sh```).

Also you can set optional cmd line arguments to set the size, targetE, specify if you want to activate the GUI or OpenMP.

#### Build:
```
qmake heat_equation_qt.pro CONFIG+=debug CONFIG+=console
make
make clean
```
#### Run:
```
./heat_equation_qt <size> <targetE> <gui ||nogui> <openmp || noopenmp>
./heat_equation_qt 101 0.01 nogui openmp
```