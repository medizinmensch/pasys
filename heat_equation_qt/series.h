#ifndef SERIES_H
#define SERIES_H

#include <QVector>
#include <frame.h>

class Series {
public:
  Series();

  /**
   * Initalises a series object which represents a development of temperature distribution over time.
   * The QVector 'frames' will get the calculated frames appended.
   *
   * @param boundarySize dimensions for the 2D area
   * @param highestPointTemperature is the highest Temperature in the area
   */
  Series(int boundarySize, double highestPointTemperature);

  /**
   * Will iterate until the given 'e' is reached which defines the convergence of the temperature.
   * The QVector 'frames' will get the calculated frames appended.
   *
   * @param e defines the minimal temperature change between the last two frames
   * @param openmp states if parallelisation should be used.
   */
  void iterateUntil(double e, bool openmp);
  int startTemp;
  int boundarySize;
  int eRemaining = 0;
  QVector<Frame> frames;
};

#endif // SERIES_H
