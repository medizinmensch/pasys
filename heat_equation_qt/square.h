#ifndef SQUARE_H
#define SQUARE_H
#include <QDebug>

class Square {

public:

  /**
   * To initialize multiple instances of square objects for a 2D array
   */
  Square();

  /**
   * Represents one pixel in the 2D array of temperature distribution.
   *
   * @param temperature contains actual value
   * @param holdStatic defines if the heat source(s) & boundaries will kept at original temperature.
   */
  Square(double temperature, bool holdStatic);

  friend QDebug operator<<(QDebug dbg, const Square &sq);
  double temperature;
  double e;
  bool holdStatic; // for borders (!=0) and heat sources
};

#endif // SQUARE_H
