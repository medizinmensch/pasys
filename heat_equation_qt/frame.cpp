#include "frame.h"
#include "square.h"

#include <QDebug>

Frame::Frame() {
}

Frame::Frame(int size) {
  this->size = size;

  Square **squaresTemp = (Square **)malloc(size * sizeof(Square *));
  for (int i = 0; i < size; i++) {
      squaresTemp[i] = (Square *)malloc(size * sizeof(Square));
    }
  this->squares = squaresTemp;
}

Frame::Frame(int size, int highestPointTemperature) {
  this->size = size;
  this->highestE = highestPointTemperature;   // e for first frame is maxTemp - 0

  Square **squaresTemp = (Square **)malloc(size * sizeof(Square *));
  for (int i = 0; i < size; i++) {
      squaresTemp[i] = (Square *)malloc(size * sizeof(Square));
  }

  // create first frame with heat sources
  for (int x = 0; x < size; x++) {
      for (int y = 0; y < size; y++) {
          Square s(0, false);
          if (y == 0 || x == 0 || x == size - 1 || y == size - 1) { // borders
              s.holdStatic = true;
          } else if (y == size / 2 && x == size / 2) { // central square (insert heat sources here)
              s.temperature = highestPointTemperature;
              s.holdStatic = true;
          }
          squaresTemp[x][y] = s;
        }
    }

  this->squares = squaresTemp;
}

Frame Frame::getNextFrame(bool debug, bool openmp) {

  Frame newFrame(size);
  newFrame.highestE = 0;
  double c = 0.2; // thermal conductivity

  // two for loops calculate values for every square in a new frame
#pragma omp parallel for private (x, y) num_threads(4) if (openmp)
  for (int x = 0; x < size; x++) {
      for (int y = 0; y < size; y++) {
          double oldTemperature = this->squares[x][y].temperature;

          if (this->squares[x][y].holdStatic) {
              newFrame.squares[x][y].temperature = this->squares[x][y].temperature;
              newFrame.squares[x][y].holdStatic = true;
          } else { // actual calculation is done here
              newFrame.squares[x][y].temperature = (oldTemperature +
                  c * (this->squares[x + 1][y].temperature + this->squares[x - 1][y].temperature - 2 * oldTemperature) +
                  c * (this->squares[x][y + 1].temperature + this->squares[x][y - 1].temperature - 2 * oldTemperature));
              newFrame.squares[x][y].holdStatic = false;
          }

          // calculate e
          newFrame.squares[x][y].e =
              newFrame.squares[x][y].temperature - oldTemperature;
          if (newFrame.squares[x][y].e > newFrame.highestE) {
              newFrame.highestE = newFrame.squares[x][y].e;
          }
      }
  }

  if (debug) {
      printFrame();
  }

  return newFrame;
}

void Frame::printFrame() {
  qDebug() << "Next Frame:";
  for (int x = 0; x < size; x++) {
      QString row = "";
      for (int y = 0; y < size; y++) {
          row.append(QString::number(this->squares[x][y].temperature) + " ");
      }
      qDebug() << row;
  }
}
