QT += widgets
requires(qtConfig(combobox))

# QMAKE_CXXFLAGS+= -fopenmp
# QMAKE_LFLAGS += -fopenmp

HEADERS       = renderarea.h \
                frame.h \
                series.h \
                square.h \
                window.h
SOURCES       = main.cpp \
                frame.cpp \
                renderarea.cpp \
                series.cpp \
                square.cpp \
                window.cpp
RESOURCES     = heat_equation.qrc

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/painting/heat_equation
INSTALLS += target
