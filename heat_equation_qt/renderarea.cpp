#include "renderarea.h"

#include <QDebug>
#include <QPainter>
#include <series.h>

RenderArea::RenderArea(int boundarySize, double e, int maxTemp, bool openmp) : series(boundarySize, maxTemp) {
  currentFrame = 0;
  availableFrames = 0;

  this->boundarySize = boundarySize;
  this->targetE = e;
  this->openmp = openmp;
}

void RenderArea::calculate() {
  series.iterateUntil(targetE, openmp);
  availableFrames = series.frames.size() - 1;
}

QSize RenderArea::minimumSizeHint() const { return QSize(100, 100); }

QSize RenderArea::sizeHint() const { return QSize(400, 200); }

QColor RenderArea::getColorRelativeToMaxTemp(double currTemp, double maxTemp) {
  QColor color;
  if (currTemp == 0) { // zero is always black
	color = QColor(0, 0, 0, 255);
  } else if (currTemp == maxTemp) { // max temp is always white
	color = QColor(255, 255, 255, 255);
  } else if (currTemp > (maxTemp * 0.6)) {
	color = QColor(255, 255, 100, 255);
  } else if (currTemp > (maxTemp * 0.4)) {
	color = QColor(255, 200, 100, 255);
  } else if (currTemp > (maxTemp * 0.25)) {
	color = QColor(255, 64, 0, 255);
  } else if (currTemp > (maxTemp * 0.1)) {
	color = QColor(192, 64, 0, 255);
  } else if (currTemp > (maxTemp * 0.05)) {
	color = QColor(192, 128, 0, 255);
  } else if (currTemp > (maxTemp * 0.025)) {
	color = QColor(128, 128, 0, 255);
  } else if (currTemp > (maxTemp * 0.01)) {
	color = QColor(64, 128, 64, 255);
  } else if (currTemp > (maxTemp * 0.005)) {
	color = QColor(0, 128, 96, 255);
  } else if (currTemp > (maxTemp * 0.001)) {
	color = QColor(0, 64, 96, 255);
  } else if (currTemp > (maxTemp * 0.0001)) {
	color = QColor(0, 64, 128, 255);
  } else if (currTemp > (maxTemp * 0.00001)) {
	color = QColor(0, 32, 96, 255);
  } else if (currTemp > (maxTemp * 0.000001)) {
	color = QColor(0, 32, 64, 255);
  } else {
	color = QColor(0, 0, 64, 255);
  }
  return color;
}

void RenderArea::setPen(const QPen &pen) {
  this->pen = pen;
  update();
}

void RenderArea::setBrush(const QBrush &brush) {
  this->brush = brush;
  update();
}

double RenderArea::frameSpinBoxChanged(const int value) {
  currentFrame = value;
  this->update();
  return this->series.frames.at(value).highestE;
}

void RenderArea::eSpinBoxChanged(const double d) { this->targetE = d; }

void RenderArea::sliderChanged() {}

void RenderArea::paintEvent(QPaintEvent * /* event */) { drawFrame(); }

void RenderArea::drawFrame() {

  QPainter painter(this);

  double min_length =
	  size().width() < size().height() ? size().width() : size().height();

  int frameSize = series.frames.at(currentFrame).size;

  int rectSize = min_length / (frameSize + 1);
  int pixelWidth = (size().width() - rectSize * frameSize) / 2;
  int pixelHeight = (size().height() - rectSize * frameSize) / 2;
  QRect rect(pixelWidth, pixelHeight, rectSize - 1, rectSize - 1);

  double startTemp = series.startTemp;

  for (int x = 0; x < frameSize; x++) {
	for (int y = 0; y < frameSize; y++) {
	  painter.save();

	  double currTemp =
		  series.frames.at(currentFrame).squares[x][y].temperature;
      QColor color = getColorRelativeToMaxTemp(currTemp, startTemp);

      painter.setBrush(QBrush(color));
      painter.setPen(QPen(color.darker(), 0));

      painter.translate(x * rectSize, y * rectSize);
      painter.drawRect(rect);
	  painter.restore();
	}
  }

  // draw frame around it
  painter.setRenderHint(QPainter::Antialiasing, false);
  painter.setPen(palette().dark().color());
  painter.setBrush(Qt::NoBrush);
  painter.drawRect(QRect(0, 0, width() - 1, height() - 1));
}
