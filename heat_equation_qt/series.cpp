#include "series.h"
#include <frame.h>
#include <square.h>

Series::Series(int boundarySize, double highestPointTemperature) {
  this->boundarySize = boundarySize;
  this->startTemp = highestPointTemperature;

  Frame newFrame(boundarySize, highestPointTemperature);
  frames.append(newFrame);
}

void Series::iterateUntil(double targetE, bool openmp) {
  int currentFrame = 0;
  bool debug = false;

  // calculates frames until convergence for given e is reached
  do {
    frames.append(frames.last().getNextFrame(debug, openmp));

	currentFrame++;
	double tmp = frames.at(currentFrame).highestE;
	if (debug) {
	  qDebug() << "currentFrame:" << currentFrame
			   << "highest e:" << frames.at(currentFrame).highestE;
	  qDebug() << "tmp(" << tmp << ") < targetE(" << targetE << ")";
	}
  } while (frames.at(currentFrame).highestE > targetE);
}
