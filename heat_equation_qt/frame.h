#ifndef FRAME_H
#define FRAME_H

#include <QVector>
#include <square.h>

class Frame {
public:
  Frame();

  /** When calling this constructor it will additionally calculate the initial frame.
  *
  * @param size gives the dimensions for a square 2D area in which the temperature distributes
  * @param highestPointTemperature is the maximum Temperature for the heat sources
  */
  Frame(int size, int highestPointTemperature);

  double highestE;
  int size;
  Square **squares;

  /** Calculates the next iteration resulting from the temperature gradient from the last frame.
  *
  * @param debug will result in a defined log output to display calculated values
  * @param openmp will be used to trigger parallelisation for nested for-loops
  * @return 2D array as a frame object
  */
  Frame getNextFrame(bool debug, bool openmp);

private:
  /** This constructor gets used to initalise a new frame.
  * The resulting values will then be calculated and set accordingly.
  *
  * @param size gives the dimensions for a square 2D area in which the temperature distributes
  */
  Frame(int size);

  /** Used for testing and debugging. Will log the current frame to the console.
  */
  void printFrame();
};

#endif // FRAME_H
