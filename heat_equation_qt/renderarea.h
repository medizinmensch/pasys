#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QBrush>
#include <QPen>
#include <QWidget>
#include <frame.h>
#include <series.h>

class RenderArea : public QWidget {
  Q_OBJECT

public:
  /**
   * A quadratic space where the temperature distribution will be visualized
   *
   * @param boundarySize
   * @param e value of temperature difference defining the convergence
   * @param maxTemp highest temperature
   * @param openmp triggers parallelisation with openmp
   */
  RenderArea(int boundarySize, double e, int maxTemp, bool openmp);

  int boundarySize;
  double targetE;
  bool openmp;

  int availableFrames;

  QSize minimumSizeHint() const override;
  QSize sizeHint() const override;
  QColor getColorRelativeToMaxTemp(double currentTemp, double maxTemp);

public slots:
  void setPen(const QPen &pen);
  void setBrush(const QBrush &brush);
  void sliderChanged();
  double frameSpinBoxChanged(const int value);
  void calculate();
  void eSpinBoxChanged(const double value);
  void drawFrame();

protected:
  void paintEvent(QPaintEvent *event) override;

private:
  Series series;
  int currentFrame;

  QPen pen;
  QBrush brush;
};

#endif // RENDERAREA_H
